$(function(){
	document.querySelector("#menuSide").style.transform = "translate(0px)"
	document.querySelector("#aside").style.transition = "all 0.8s"
	document.querySelector("#menuSide").style.transition = "all 0.8s"
	document.querySelector("#menu").addEventListener("click", function(){
		
		if(document.querySelector("#menuSide").style.transform == "translate(0px)"){
			document.querySelector("#menuSide").style.transform = "translate(-300px)"
			document.querySelector("#aside").style.width = "0px"
		}else{
			document.querySelector("#menuSide").style.transform = "translate(0px)"
			document.querySelector("#aside").style.width = "300px"
		}
	})
})