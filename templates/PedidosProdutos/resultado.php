<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Resultado</div>
        <div>
            <?= $this->Html->link(__('Nova Consulta'), ['controller' => 'pedidosProdutos', 'action' => 'consulta'], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <?= $this->element('form');?>
    <?= $this->element('pagination');?>
</div>
<div class="space"></div>

