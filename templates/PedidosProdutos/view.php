<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Visualizar</div>
        <div>
            <?= $this->Html->link(__('Lista Pedidos'), ['controller' => 'pedidosProdutos', 'action' => 'index'], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    <hr/>
    <table class='table table-borderless table-hover table-sm'>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= $this->Number->format($pedidosProduto->id) ?></th>
        </tr>
        <tr>
            <th><?= __('Categoria') ?></th>
            <td><?= h($pedidosProduto->cat_pai) ?></td>
        </tr>
        <tr>
            <th><?= __('Sub-Categoria') ?></th>
            <td><?= h($pedidosProduto->nome_cat) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantidade') ?></th>
            <td><?= $this->Number->format($pedidosProduto->quantidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Movimento') ?></th>
            <td>
                <?php 
                    if($this->Number->format($pedidosProduto->action) == 0){
                        echo "<span style='color:green;'>Entrada</span>";
                    }elseif($this->Number->format($pedidosProduto->action) == 1){
                        echo "<span style='color:red;'>Retirada</span>";
                    }else{
                        echo "<span style='color:orange;'>Retirada</span>";
                    } 
                ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Data Criação') ?></th>
            <td><?= h($pedidosProduto->created) ?></td>
        </tr>
    </table>
</div>
<div class="space"></div>
