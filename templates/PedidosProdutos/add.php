<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Novo Pedido</div>
        <div>
            <?= $this->Html->link(__('Lista Pedidos'), ['controller' => 'pedidosProdutos', 'action' => 'index'], ['class' => 'btn btnTop']) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr>
    <?= $this->Form->create($pedidosProduto) ?>
        <div class="form-group">
            <h5>
                <label for='nome_cat'> Categoria</label>
            </h5>
            <?= $this->Form->select('nome_cat', $cat, ['label'=>false, 'class' => 'form-control', 'id' => 'nome_cat']);?>
        </div>
        
        <div class="form-group">
            <h5>
                <label for='quant'> Quantidade</label>
            </h5>
            <?= $this->Form->control('quant', ['label'=>false, 'class' => 'form-control', 'id' => 'quant']);?>
        </div>
        <div class="form-group">
            <h5>
                <label for='mov'> Movimentação</label>
            </h5>
            <?= $this->Form->select('action', ['Entrada', 'Retirada', 'Devolução'], ['empty' => 'Movimento', 'label' => false, 'class' => 'form-control', 'id' => 'mov']);?>
        </div>
        <div class="form-group">
            <div class="space"></div>
            <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-warning']) ?>
        </div>
        <?= $this->Form->end() ?>
</div>
<div class="space"></div>
