<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Consulta</div>
        <div>
            <?= $this->Html->link(__('Lista Pedidos'), ['controller' => 'pedidosProdutos', 'action' => 'index'], ['class' => 'btn btnTop']) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <div class="flex flex-center">
        <?= $this->Form->create() ?>
        <div class="form-group col-md-8">
            <h5>
                <label for="ac">Movimentação</label>
            </h5>
            <?= $this->Form->select('acao', ['Entrada', 'Retirada', 'Devolução'], ['label' => false, 'type' => 'number', 'class' => 'form-control', 'id' => 'ac'])?>
        </div>
        <div class="spaceDois"></div>
        <div class="flex flex-start">
            <div class="form-group col-md-6">
                <h5>
                    <label for="dti">Data Início</label>
                </h5>
                <?= $this->Form->dateTime('dataInicio', ['class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-6">
                <h5>
                    <label for="dtf">Data Final</label>
                </h5>
                <?= $this->Form->dateTime('dataFim', ['class' => 'form-control'])?>
            </div>
        </div>
        <div class="spaceDois"></div>
        <?= $this->Form->button(__('Enviar'), ['class' => 'btn btn-warning']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="space"></div>

