<?php

$name = $this->request->getSession()->read('Auth.name');
$id = $this->request->getSession()->read('Auth.id');

?>
<div class="conteudo">
    <div class="btn-group">
        <div class="usuario btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= $this->Html->image("../img/logon3.png")?>
            <?= ucfirst($name)?>
        </div>
        <div class="dropdown-menu">
            <?= $this->Html->link('<i class="fas fa-user"></i> Perfil', ['controller' => 'Users', 'action' => 'view', $id], ['class' => 'dropdown-item', 'escape' => false])?>
            <div class="dropdown-divider"></div>
            <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Sair', ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item', 'escape' => false])?>
        </div>
    </div>
</div>