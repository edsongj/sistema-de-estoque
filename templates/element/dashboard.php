
<nav>
    <div id="menuSide">
        <ul>
            <li>
                <?= $this->Html->link(
                                        '<i class="fas fa-tachometer-alt"></i> Dashboard',
                                        [
                                            'controller' => 'Home','action' => 'index'
                                        ],
                                        [
                                            // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                                            'escape' => false
                                        ]

                                    );
                ?>
            </li>
            <li>
                <?= $this->Html->link(
                                        '<i class="fas fa-cogs"></i></i> Categorias',
                                        [
                                            'controller' => 'Produtos','action' => 'index'
                                        ],
                                        [
                                            // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                                            'escape' => false
                                        ]

                                    );
                ?>
            </li>
            <li>
                <?= $this->Html->link(
                                        '<i class="fas fa-tasks"></i> Pedidos',
                                        [
                                            'controller' => 'pedidosProdutos','action' => 'index'
                                        ],
                                        [
                                            // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                                            'escape' => false
                                        ]

                                    );
                ?>
            </li>
            <li>
                <?= $this->Html->link(
                                        '<i class="far fa-calendar-alt"></i> Consulta',
                                        [
                                            'controller' => 'pedidosProdutos','action' => 'consulta'
                                        ],
                                        [
                                            // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                                            'escape' => false
                                        ]

                                    );
                ?>
            </li>


            <li>
                <?= $this->Html->link(
                                        '<i class="fas fa-sign-out-alt"></i> Sair',
                                        [
                                            'controller' => 'users','action' => 'logout'
                                        ],
                                        [
                                            // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                                            'escape' => false
                                        ]

                                    );
                ?>
            </li>



        </ul>
    </div>
</nav>