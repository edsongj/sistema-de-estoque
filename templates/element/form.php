<table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Categoria</th>
                <th>Sub-Categoria</th>
                <th>Quantidade</th>
                <th>Movimento</th>
                <th>Data Criação</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedidosProdutos as $pedidosProduto): ?>
            <tr>
                <td><?= $this->Number->format($pedidosProduto->id) ?></td>
                <td><?= h($pedidosProduto->cat_pai) ?></td>
                <td><?= h($pedidosProduto->nome_cat) ?></td>
                <td><?= $this->Number->format($pedidosProduto->quant) ?></td>
                <td>
                    <?php 
                        if($this->Number->format($pedidosProduto->action) == 0){
                            echo "<span style='color:green;'>Entrada</span>";
                        }elseif($this->Number->format($pedidosProduto->action) == 1){
                            echo "<span style='color:red;'>Retirada</span>";
                        }else{
                            echo "<span style='color:orange;'>Devolução</span>";
                        } 
                    ?>
                </td>
                <td><?= h($pedidosProduto->created) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>