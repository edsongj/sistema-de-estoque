<?php
    // echo "<pre>"; 
    // var_dump($name);
    // echo "</pre>"; 

?>
<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">
            <h1>Controle de Estoque<h1>
        </div>
        <div>
            <?= $this->Html->link(__('Sair'), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'btn btn-warning']) ?>
        </div>
    </div>
    <div class="spaceDois"></div>
    <hr>
    <div class="spaceDois"></div>
    <div class="flex flex-space-around">
        <?= $this->Html->link(__('Categorias'), ['controller' => 'Produtos','action' => 'index'], ['class' => 'btn btn-info btn-lg']) ?>
        <?= $this->Html->link(__('Nova Categoria'), ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-info btn-lg']) ?>
    </div>
    <div class="spaceDois"></div>
    <div class="flex flex-space-around">
        <?= $this->Html->link(__('Pedidos Produtos'), ['controller' => 'pedidosProdutos','action' => 'index'], ['class' => 'btn btn-info btn-lg']) ?>
        <?= $this->Html->link(__('Nova Movimentação'), ['controller' => 'pedidosProdutos', 'action' => 'add'], ['class' => 'btn btn-info btn-lg']) ?>
    </div>
    <div class="spaceDois"></div>
    <div class="flex flex-space-around">
        <?= $this->Html->link(__('Consulta Movimentação'), ['controller' => 'pedidosProdutos', 'action' => 'consulta'], ['class' => 'btn btn-info btn-lg']) ?>
    </div>
   
    
</div>