<div class="content">
    <div class="title_d">Editar Categoria</div>
    <div class="flex flex-space-between">
        <?= $this->Html->link(__('Lista Categorias'), ['action' => 'index'], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink( __('Delete'), ['action' => 'delete', $produto->id], ['class' => 'btn btn-danger', 'confirm' => __('Você tem certeza que deseja excluir a Categoria: {0}?', $produto->name_cat)]) ?>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <?= $this->Form->create($produto) ?>
    <div class="form-group">
        <h5>
            <label for="cat">Categoria</label>
        </h5>
        <?= $this->Form->control('parent_id', ['options' => $parentProdutos, 'label' => false, 'class' => 'form-control', 'id' => 'cat']);?>
    </div>
    <div class="form-group">
        <h5>
            <label for="sub">Sub-Categoria</label>
        </h5>
        <?= $this->Form->control('name_cat', ['label' => false, 'class' => 'form-control', 'id'=>'sub']);?>
    </div>
    <div class="form-group">
        <h5>
            <label for="desc">Descrição</label>
        </h5>
        <?= $this->Form->control('description', ['label' => false, 'class' => 'form-control', 'rows' => 2, 'id' => 'desc']);?>
    </div>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-warning']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="space"></div>
