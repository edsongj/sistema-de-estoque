<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Visualizar</div>
        <div>
            <?= $this->Html->link(__('Listar Categorias'), ['action' => 'index'], ['class' => 'btn btn-info']) ?>
            <?= $this->Html->link(__('Editar Categoria'), ['action' => 'edit', $produto->id], ['class' => 'btn btn-warning']) ?>
            <?= $this->Form->postLink(__('Deletar Categoria'), ['action' => 'delete', $produto->id], ['class' => 'btn btn-danger', 'confirm' => __('Você tem certeza que deseja excluir a Categoria: {0}?', $produto->name_cat)]) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <div class="content-table">
        <table class='table table-borderless table-hover table-sm'>
            <tr>
                <th><?= __('ID') ?></th>
                <td><?= $this->Number->format($produto->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Categoria') ?></th>
                <td><?= h($produto->name_cat) ?></td>
            </tr>
            <tr>
                <th><?= __('Quantidade') ?></th>
                <td><?= $this->Number->format($produto->quantidade) ?></td>
            </tr>
            <tr>
                <th><?= __('Data Criação') ?></th>
                <td><?= h($produto->created) ?></td>
            </tr>
            <tr>
                <th><?= __('Data Modificação') ?></th>
                <td><?= h($produto->modified) ?></td>
            </tr>
        </table>
        <div class="flex wrap">
            <strong><?= __('Description') ?></strong>
            <blockquote>
                <?= $this->Text->autoParagraph(h($produto->description)); ?>
            </blockquote>
        </div>
    </div>
</div>
<div class="space"></div>
