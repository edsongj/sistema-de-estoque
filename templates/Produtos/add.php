<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Nova Categoria</div>
        <div>
            <?= $this->Html->link(__('Lista Categorias'), ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btnTop']) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <?= $this->Form->create($produto) ?>
        <div class="form-group">
            <h5>
                <label for="cat">Categoria</label>
            </h5>
            <?= $this->Form->control('parent_id', ['options' => $parentProdutos, 'label' => false, 'class' => 'form-control', 'id' => 'cat']);?>
        </div>
        <div class="form-group">
            <h5>
                <label for="sub">Sub-Categoria</label>
            </h5>
            <?= $this->Form->control('name_cat', ['label' => false, 'class' => 'form-control', 'id' => 'sub']);?>
        </div>
        <div class="form-group">
            <h5>
                <label for="desc">Descrição</label>
            </h5>
            <?= $this->Form->control('description', ['label' => false, 'class' => 'form-control', 'rows' => 2, 'id' => 'desc']);?>
        </div>
    <div class="space"></div>
    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-warning']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="space"></div>
