<div class="content">
    <div class="flex flex-space-between">
        <div class="title_d">Categorias</div>
        <div>
            <?= $this->Html->link(__('Nova Categoria'), ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btnTop']) ?>
        </div>
    </div>
    <?= $this->Flash->render();?>
    <hr/>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Categoria</th>
                <th>quantidade</th>
                <th>Data Criação</th>
                <th>Data Modificação</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produtos as $produto): ?>
            <tr>
                <td><?= $this->Number->format($produto->id) ?></td>
                <td><?= h($produto->name_cat) ?></td>
                <td><?= $this->Number->format($produto->quantidade) ?></td>
                <td><?= h($produto->created) ?></td>
                <td><?= h($produto->modified) ?></td>
                <td>
                <?php
                    if($produto->id != 1):?>
                        <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $produto->id], ['class' => 'btn btn-primary btn-sm']) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $produto->id], ['class' => 'btn btn-warning btn-sm']) ?>
                        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $produto->id], ['class' => 'btn btn-danger btn-sm', 'confirm' => __('Você tem certeza que deseja excluir a Categoria: {0}?', $produto->name_cat)]) ?>
                <?php endif?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->element('pagination');?>
</div>
<div class="space"></div>