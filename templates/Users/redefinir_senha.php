<div class="conteudo">
    <?= $this->Form->create() ?>
        <?= $this->Flash->render()?>
        <legend><?= __('Redefinir Senha') ?></legend>
        <p>Informe seu usuário</p>
        <div class="form-col col-6">
            <?= $this->Form->control('name',['label'=>false, 'class' => 'form-control']);?>
        </div>
        <div class="space">
            <?= $this->Form->button(__('Continuar'), ['class' => 'btn btn-warning']) ?>
            <?= $this->Html->link(__('Voltar'), ['action'=>'index'], ['class' => 'btn btn-primary']) ?>
        </div>
    <?= $this->Form->end() ?>
</div>
