<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="content">
    <div class="flex flex-space-between">
        <div class="column-responsive column-80">
            <table class="spaceQuatro">
                <tr>
                    <th class="spaceTres"> <h2><?= __('Id') ?></h2></th>
                    <td><h2><?= $this->Number->format($user->id) ?></h2></td>
                </tr>
                <tr>
                    <th class="spaceTres"><h3><?= __('Usuário') ?></h3></th>
                    <td><h3><?= h($user->name) ?></h3></td>
                </tr>
                <tr>
                    <th class="spaceTres"><h3><?= __('Data de Criação') ?></h3></th>
                    <td><h3><?= h($user->created) ?></h3></td>
                </tr>
            </table>
        </div>
        <div>
            <?= $this->Html->link(__('Editar Perfil'), ['action' => 'editarPerfil', $user->id], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
</div>
