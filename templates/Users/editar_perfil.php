<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="content">
    <div class="column-responsive column-80">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Editar Perfil') ?></legend>
            <div class="spaceSeis"></div>
            <label for="name">Usuário</label>
            <?= $this->Form->control('name', ['label' => false, 'class' => 'form-control col-5', 'id' => 'name', 'value' => '']);?>
            <div class="spaceQuatro"></div>
            <label for="password">Senha</label>
            <?= $this->Form->control('password', ['label' => false, 'class' => 'form-control col-5', 'id' => 'password', 'value' => '']);?>
        </fieldset>
        <div class='spaceQuatro'></div>
        <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
