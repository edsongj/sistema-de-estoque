<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */

/*  
    # Não é possível acessar essa variável antes do logar no sistema, 
    # caso contrário seria falha de segurança do framework
*/
// echo "<pre>";
// var_dump($name);
// echo "</pre>";
?>
<div class="content">
    <div class="mascara"></div>
    <div class="login">
        <div class="containe-form">
            <img src="../img/logon2.png" alt="Login">
            <div class="message">
                <?= $this->Flash->render() ?>
            </div>
            <?= $this->Form->create() ?>
            <fieldset>
                <label for='username'>Username</label>
                <?= $this->Form->control('name', ['label' => false, 
                'class' => 'form-control', 'id' => 'username']);?>
                <label for="pass">Password</label>
                <?= $this->Form->control('password', ['label' => false, 
                'class' => 'form-control', 'id' => 'password']);?>
            </fieldset>
            <div class="botao">
                <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-success']) ?>
                <?= $this->Html->link(__('Esqueceu a senha?'), ['controller' => 'Users', 'action' => 'redefinirSenha'], ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
