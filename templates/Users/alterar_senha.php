<div class="conteudo">
    <div class="message">
        <?= $this->Flash->render()?>
    </div>
    <?= $this->Form->create() ?>
        <legend><?= __('Nova Senha') ?></legend>
        <div class="form-col col-6">
            <?= $this->Form->control('password', ['label'=>false, 'class' => 'form-control']);?>
        </div>
    <div class="space">
        <?= $this->Form->button(__('Confirmar'), ['class' => 'btn btn-success']) ?>
        <?= $this->Html->link(__('Voltar'), ['action'=>'redefinir_senha'], ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
