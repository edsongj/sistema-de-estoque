<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PedidosProdutos Model
 *
 * @method \App\Model\Entity\PedidosProduto get($primaryKey, $options = [])
 * @method \App\Model\Entity\PedidosProduto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PedidosProduto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PedidosProduto|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PedidosProduto saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PedidosProduto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PedidosProduto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PedidosProduto findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PedidosProdutosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('pedidos_produtos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('cat_pai')
            ->maxLength('cat_pai', 220)
            ->requirePresence('cat_pai', 'create')
            ->notEmptyString('cat_pai');

        $validator
            ->scalar('nome_cat')
            ->maxLength('nome_cat', 220)
            ->requirePresence('nome_cat', 'create')
            ->notEmptyString('nome_cat');

        $validator
            ->integer('quant')
            ->requirePresence('quant', 'create')
            ->notEmptyString('quant');

        $validator
            ->integer('action')
            ->requirePresence('action', 'create')
            ->notEmptyString('action');

        return $validator;
    }
    public function getListaPedidos($action, $dataIni, $dataFim)
    {
        $query = $this->find()
                    ->select(['id', 'cat_pai', 'nome_cat', 'quant', 'action', 'created'])
                    ->where(['action =' => $action])
                    ->where([ 'created >=' => $dataIni, 'created <=' => $dataFim]);
        return $query;
    }
}
