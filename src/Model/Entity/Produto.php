<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Produto Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property string $name_cat
 * @property int|null $quantidade
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $description
 *
 * @property \App\Model\Entity\ParentProduto $parent_produto
 * @property \App\Model\Entity\ChildProduto[] $child_produtos
 */
class Produto extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'name_cat' => true,
        'quantidade' => true,
        'created' => true,
        'modified' => true,
        'description' => true,
        'parent_produto' => true,
        'child_produtos' => true,
    ];
}
