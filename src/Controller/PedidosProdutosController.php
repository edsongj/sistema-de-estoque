<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * PedidosProdutos Controller
 *
 * @property \App\Model\Table\PedidosProdutosTable $PedidosProdutos
 *
 * @method \App\Model\Entity\PedidosProduto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PedidosProdutosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 5
        ];
        $pedidosProdutos = $this->paginate($this->PedidosProdutos);

        $this->set(compact('pedidosProdutos'));
    }

    /**
     * View method
     *
     * @param string|null $id Pedidos Produto id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pedidosProduto = $this->PedidosProdutos->get($id, [
            'contain' => [],
        ]);

        $this->set('pedidosProduto', $pedidosProduto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        /*
        # Utilizando o método para pegar os elementos do campo nome_cat de categorias, 
        # envia os dados p/ PedidosProdutos\add.php
        */
        $categoriasTable = TableRegistry::get('Produtos');
        $cat = $categoriasTable->getCategorias();
        $pedidosProduto = $this->PedidosProdutos->newEmptyEntity();
        if ($this->request->is('post')) {
            $pedidosProduto = $this->PedidosProdutos->patchEntity($pedidosProduto, $this->request->getData());
            $parentCat = $categoriasTable->getParentCat($pedidosProduto->nome_cat);
            $nomeCat = $categoriasTable->getNomeCat($parentCat->parent_id);
            $pedidosProduto->cat_pai = $nomeCat->name_cat;
            if ($this->PedidosProdutos->save($pedidosProduto)) {
                $this->Flash->success(__('Movimentação realizada com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: Movimentação não foi realizada com sucesso'));
        }
        $this->set(compact('pedidosProduto', 'cat'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pedidos Produto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        return $this->redirect(['action' => 'index']);
        $pedidosProduto = $this->PedidosProdutos->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pedidosProduto = $this->PedidosProdutos->patchEntity($pedidosProduto, $this->request->getData());
            if ($this->PedidosProdutos->save($pedidosProduto)) {
                $this->Flash->success(__('The pedidos produto has been saved.'));

            }
            $this->Flash->error(__('The pedidos produto could not be saved. Please, try again.'));
        }
        $this->set(compact('pedidosProduto'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pedidos Produto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pedidosProduto = $this->PedidosProdutos->get($id);
        if ($this->PedidosProdutos->delete($pedidosProduto)) {
            $this->Flash->success(__('The pedidos produto has been deleted.'));
        } else {
            $this->Flash->error(__('The pedidos produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function consulta()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->getData();
            $dataInicio = $this->confData($dados['dataInicio']);
            $dataFim = $this->confData($dados['dataFim']);
            return $this->redirect(['action' => 'resultado', $dados['acao'], $dataInicio, $dataFim]);
        }
    }
    public function resultado($action = null, $dataInicio = null, $dataFim = null)
    {
        $pedidosProdutos = $this->PedidosProdutos->getListaPedidos($action, $dataInicio, $dataFim);
        $this->paginate = [
            'limit' => 10
        ];
        $pedidosProdutos = $this->paginate($pedidosProdutos);
        $this->set(compact('pedidosProdutos'));

    }
    public function confData($dataOrigin = null)
    {
        $dataAterada = explode("T", $dataOrigin);
        $dataAterada = implode(" ",$dataAterada);
        $dataAterada = explode(".000", $dataAterada);
        $dataAterada = implode("",$dataAterada);
        return $dataAterada;
    }
}
