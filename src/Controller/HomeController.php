<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Produtos Controller
 *
 * @property \App\Model\Table\ProdutosTable $Produtos
 *
 * @method \App\Model\Entity\Produto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $hello = "Olá";
        $this->viewBuilder()->setLayout('index');

        // $name = $this->request->getSession();
        // $name = $this->request->getSession()->read();
        // $name = $this->request->getSession()->read('Auth.id');
        // $name = $name['Auth'];
        // $name = $name['Auth']['name'];
        // $name = $name['Auth']['id'];
        // $this->set(compact('hello', 'info'));
        // $this->set(compact('name', 'hello', 'info'));
    }
}
